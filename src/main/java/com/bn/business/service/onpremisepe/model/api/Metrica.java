package com.bn.business.service.onpremisepe.model.api;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.Past;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(description="All details about the Metricas")
@Entity
public class Metrica {

	@Id
	@GeneratedValue
	private int id;
	
	@Past
	@ApiModelProperty(notes="The value must be positive")
	private double porcentajeAvance;
	
	@Past
	@ApiModelProperty(notes="The value must be positive")
	private double costo;
	
	@Past
	@ApiModelProperty(notes="The value must be positive")
	private String status;
	
	@Past
	@ApiModelProperty(notes="The value must be positive")
	private String proyecto;

	public Metrica(int id, double porcentajeAvance, double costo, String status, String proyecto) {
		super();
		this.id = id;
		this.porcentajeAvance = porcentajeAvance;
		this.costo = costo;
		this.status = status;
		this.proyecto = proyecto;
	}

	public Metrica() {
		super();
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public double getPorcentajeAvance() {
		return porcentajeAvance;
	}

	public void setPorcentajeAvance(double porcentajeAvance) {
		this.porcentajeAvance = porcentajeAvance;
	}

	public double getCosto() {
		return costo;
	}

	public void setCosto(double costo) {
		this.costo = costo;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getProyecto() {
		return proyecto;
	}

	public void setProyecto(String proyecto) {
		this.proyecto = proyecto;
	}
	
}
